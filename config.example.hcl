address = ":9610"

log_level = "DEBUG"

allow_origins = ["*"]

tls {
  enable = true

  # automatically fetch certificate via letsencrypt
  auto = true

  # or manually set certificate
  cert_file = "/tls.crt"
  key_file = "/tls.key"
}

postgres {
  address   = "localhost:5432"
  username = "lightning"
  password  = "lightning"
  database = "lightning"
}


jwt {
  # secret used by the jwt token
  # generate by running openssl rand -base64 32
  secret = ""
}

lxd {
  addr = "unix:///var/lib/lxd/unix.socket"
}
