package client

import (
	"bytes"
	"encoding/json"
	"fmt"

	"github.com/google/jsonapi"

	"gitlab.com/openvm/openvm/api"
)

// Login performs login request, sets the client token and returns it for storage
func (c *Client) Login(name, password string) (string, error) {
	req := api.UserLoginPost{
		Name:     name,
		Password: password,
	}

	payload, err := jsonapi.Marshal(&req)
	if err != nil {
		return "", err
	}

	buf := new(bytes.Buffer)
	json.NewEncoder(buf).Encode(payload)

	r, err := c.request("POST", "/login", buf)
	if err != nil {
		return "", err
	}

	switch r.StatusCode {
	case 200:
		var res api.UserLogin
		if err := jsonapi.UnmarshalPayload(r.Body, &res); err != nil {
			return "", err
		}
		c.token = res.Token
		return res.Token, nil
	default:
		// TODO unmarshal error
		return "", fmt.Errorf("unexpected response: %s", r.Status)
	}
}
