package client

import (
	"fmt"

	"github.com/google/jsonapi"
	"gitlab.com/openvm/openvm/model"
)

// UserInfo retrieves the current user's info
func (c *Client) UserInfo() (*model.User, error) {
	r, err := c.request("GET", "/userinfo", nil)
	if err != nil {
		return nil, err
	}

	switch r.StatusCode {
	case 200:
		var res model.User
		if err := jsonapi.UnmarshalPayload(r.Body, &res); err != nil {
			return nil, err
		}
		return &res, nil
	default:
		// TODO unmarshal error
		return nil, fmt.Errorf("unexpected response: %s", r.Status)
	}
}
