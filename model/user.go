package model

import "time"

type User struct {
	Id            int64  `jsonapi:"primary,users"`
	Admin         bool   `jsonapi:"attr,admin"`
	Name          string `jsonapi:"attr,name,omitempty" valid:"alphanum,length(4|20)"`
	EmailVerified bool   `jsonapi:"attr,email_verified"`
	Email         string `jsonapi:"attr,email,omitempty" valid:"email"`
	PasswordHash  string
	CreatedAt     time.Time `jsonapi:"attr,created_at"`
	UpdatedAt     time.Time `jsonapi:"attr,updated_at"`
}
