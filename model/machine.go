package model

type Machine struct {
	Id     int64         `jsonapi:"primary,machines"`
	Name   string        `jsonapi:"attr,name" valid:"printableascii,length(4|50),required"`
	Image  string        `jsonapi:"attr,image" valid:"printableascii,length(4|50),required"`
	Driver string        `jsonapi:"attr,driver" valid:"alpha,required"`
	UserId int64         `jsonapi:"attr,user_id"`
	Status MachineStatus `jsonapi:"attr,status" sql:"-"`
	Stats  MachineStats  `jsonapi:"attr,stats" sql:"-"`
}

// MachineProperties defines properties used for machine creation
type MachineProperties struct {
	Name    string
	Image   string
	RamSize int64
	CpuNum  int
	Status  MachineStatus
}

type MachineStats struct {
	Processes   int64 `jsonapi:"attr,user_id"`
	MemoryUsage int64 `jsonapi:"attr,memory_usage"`
	CpuUsage    int64 `jsonapi:"attr,cpu_usage"`
}

type MachineStatus string

const (
	Running MachineStatus = "running"
	Stopped MachineStatus = "stopped"
)
