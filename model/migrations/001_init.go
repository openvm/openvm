package migrations

import (
	"fmt"

	"github.com/dchest/uniuri"
	"github.com/go-pg/migrations"
	"golang.org/x/crypto/bcrypt"
)

func init() {
	migrations.Register(func(db migrations.DB) error {
		fmt.Println("creating table machines...")
		if _, err := db.Exec(`
			CREATE TABLE machines (
				id bigserial,
				name text,
				image text,
				driver text,
				user_id bigserial,
				node_id text,
				PRIMARY KEY (id)
			)
		`); err != nil {
			return err
		}
		fmt.Println("creating table users...")
		if _, err := db.Exec(`
			CREATE TABLE users (
				id bigserial,
				name text,
				admin bool,
				email_verified bool,
				email text,
				password_hash text,
				created_at time,
				updated_at time,
				PRIMARY KEY (id)
			)
		`); err != nil {
			return err
		}

		pass := uniuri.New()
		p, err := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost)
		if err != nil {
			return err
		}
		if _, err := db.Exec(`
			INSERT INTO users VALUES (
				1,
				'admin',
				true,
				true,
				'admin@example.com',
				'` + string(p) + `'
			)
		`); err != nil {
			return err
		}

		fmt.Printf("created user 'admin' with password '%s'\n", pass)
		return nil
	}, func(db migrations.DB) error {
		fmt.Println("dropping table machines...")
		if _, err := db.Exec(`DROP TABLE machines`); err != nil {
			return err
		}
		fmt.Println("dropping table users...")
		if _, err := db.Exec(`DROP TABLE users`); err != nil {
			return err
		}
		return nil
	})
}
