package main

import (
	"log"
	"os"

	"gitlab.com/openvm/openvm/command"

	"github.com/mitchellh/cli"
)

func main() {
	c := cli.NewCLI("openvm", "1.0.0")
	c.Args = os.Args[1:]

	ui := &cli.BasicUi{Writer: os.Stdout, ErrorWriter: os.Stderr}

	c.Commands = map[string]cli.CommandFactory{
		"delete": func() (cli.Command, error) {
			return command.DeleteCommand{Ui: ui}, nil
		},
		"launch": func() (cli.Command, error) {
			return command.LaunchCommand{Ui: ui}, nil
		},
		"list": func() (cli.Command, error) {
			return command.ListCommand{Ui: ui}, nil
		},
		"login": func() (cli.Command, error) {
			return command.LoginCommand{Ui: ui}, nil
		},
		"migrate": func() (cli.Command, error) {
			return command.MigrateCommand{Ui: ui}, nil
		},
		"server": func() (cli.Command, error) {
			return command.ServerCommand{Ui: ui}, nil
		},
		"shell": func() (cli.Command, error) {
			return command.ShellCommand{Ui: ui}, nil
		},
	}

	exitStatus, err := c.Run()
	if err != nil {
		log.Println(err)
	}

	os.Exit(exitStatus)

}
