package command

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"github.com/mitchellh/cli"
	"github.com/olekukonko/tablewriter"
	"gitlab.com/openvm/openvm/client"
)

type ListCommand struct {
	Ui cli.Ui
}

func (c ListCommand) Run(args []string) int {
	flags := flag.NewFlagSet("list", flag.PanicOnError)
	flags.Usage = func() { c.Ui.Output(c.Help()) }
	if err := flags.Parse(args); err != nil {
		return 1
	}

	client := client.New("TODO", "TODO")
	machines, err := client.MachineList()

	if err != nil {
		c.Ui.Error(fmt.Sprintf("Error getting list of machines: %s", err))
		return 1
	}

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Name", "Image"})

	for _, v := range machines {
		table.Append([]string{v.Name, v.Image})
	}
	table.Render()

	return 0
}

func (c ListCommand) Help() string {
	helpText := `
Usage: openvm list

  List all machines.
	`
	return strings.TrimSpace(helpText)
}

func (c ListCommand) Synopsis() string {
	return "List all machines"
}
