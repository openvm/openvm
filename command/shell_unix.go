// +build !windows

package command

import (
	"os"
	"os/signal"
	"strconv"
	"syscall"

	"github.com/lxc/lxd/shared/termios"
	"gitlab.com/openvm/openvm/container"
)

func controlHandler(c chan container.SessionControlMessage) {
	ch := make(chan os.Signal, 10)
	signal.Notify(ch,
		syscall.SIGWINCH,
		syscall.SIGTERM,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGQUIT,
		syscall.SIGABRT,
		syscall.SIGTSTP,
		syscall.SIGTTIN,
		syscall.SIGTTOU,
		syscall.SIGUSR1,
		syscall.SIGUSR2,
		syscall.SIGSEGV,
		syscall.SIGCONT)

	for sig := range ch {
		switch sig {
		case syscall.SIGWINCH:
			width, height, err := termios.GetSize(int(syscall.Stdout))
			if err != nil {
				panic(err)
			}
			args := map[string]string{
				"width":  strconv.Itoa(width),
				"height": strconv.Itoa(height),
			}
			c <- container.SessionControlMessage{Command: "window-resize", Args: args}
		case syscall.SIGTERM:
			c <- container.SessionControlMessage{Command: "signal", Signal: int(syscall.SIGTERM)}
		case syscall.SIGHUP:
			c <- container.SessionControlMessage{Command: "signal", Signal: int(syscall.SIGHUP)}
		case syscall.SIGINT:
			c <- container.SessionControlMessage{Command: "signal", Signal: int(syscall.SIGINT)}
		case syscall.SIGQUIT:
			c <- container.SessionControlMessage{Command: "signal", Signal: int(syscall.SIGQUIT)}
		case syscall.SIGABRT:
			c <- container.SessionControlMessage{Command: "signal", Signal: int(syscall.SIGABRT)}
		case syscall.SIGTSTP:
			c <- container.SessionControlMessage{Command: "signal", Signal: int(syscall.SIGTSTP)}
		case syscall.SIGTTIN:
			c <- container.SessionControlMessage{Command: "signal", Signal: int(syscall.SIGTTIN)}
		case syscall.SIGTTOU:
			c <- container.SessionControlMessage{Command: "signal", Signal: int(syscall.SIGTTOU)}
		case syscall.SIGUSR1:
			c <- container.SessionControlMessage{Command: "signal", Signal: int(syscall.SIGUSR1)}
		case syscall.SIGUSR2:
			c <- container.SessionControlMessage{Command: "signal", Signal: int(syscall.SIGUSR2)}
		case syscall.SIGSEGV:
			c <- container.SessionControlMessage{Command: "signal", Signal: int(syscall.SIGSEGV)}
		case syscall.SIGCONT:
			c <- container.SessionControlMessage{Command: "signal", Signal: int(syscall.SIGCONT)}
		default:
			break
		}
	}
}
