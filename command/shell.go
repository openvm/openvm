package command

import (
	"flag"
	"fmt"
	"os"
	"strings"
	"syscall"

	"github.com/lxc/lxd/shared/termios"
	"github.com/mitchellh/cli"
	"gitlab.com/openvm/openvm/client"
)

type ShellCommand struct {
	Ui cli.Ui
}

func (c ShellCommand) Run(args []string) int {
	flags := flag.NewFlagSet("shell", flag.PanicOnError)
	flags.Usage = func() { c.Ui.Output(c.Help()) }
	if err := flags.Parse(args); err != nil {
		return 1
	}

	args = flags.Args()
	if len(args) != 1 || len(args[0]) == 0 {
		c.Ui.Error("shell expects one argument")
		flags.Usage()
		return 1
	}

	machine := args[0]

	client := client.New("TODO", "TODO")

	width, height, err := termios.GetSize(int(syscall.Stdout))
	if err != nil {
		c.Ui.Error(fmt.Sprintf("Error creating shell session: %s", err))
		return 1
	}

	sessionID, err := client.SessionCreate(machine, width, height)
	if err != nil {
		c.Ui.Error(fmt.Sprintf("Error creating shell session: %s", err))
		return 1
	}

	control, err := client.SessionControl(sessionID)
	if err != nil {
		c.Ui.Error(fmt.Sprintf("Error creating shell session: %s", err))
		return 1
	}

	go controlHandler(control)

	if err := client.SessionIO(sessionID, os.Stdin, os.Stdout); err != nil {
		c.Ui.Error(fmt.Sprintf("Error creating shell session: %s", err))
		return 1
	}

	return 0
}

func (c ShellCommand) Help() string {
	helpText := `
Usage: openvm shell [options] machine

  Access the terminal of a machine.
	`
	return strings.TrimSpace(helpText)
}

func (c ShellCommand) Synopsis() string {
	return "Access the terminal of a machine"
}
