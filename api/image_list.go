package api

import "gitlab.com/openvm/openvm/model"

func (h *Handler) ImageList(r Request) Response {

	images, err := h.Container.Images()
	if err != nil {
		return Response{InternalError: err}
	}

	res := make([]*model.Image, len(images))
	for i, img := range images {
		res[i] = &model.Image{
			Fingerprint: img.Fingerprint,
			Aliases:     img.Aliases,
			Properties:  img.Properties,
		}
	}

	return Response{Resource: res}
}
