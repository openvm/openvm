package api

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/openvm/openvm/model"

	jwt "github.com/dgrijalva/jwt-go"
	jwtmiddleware "github.com/jgillich/jwt-middleware"
	"github.com/julienschmidt/httprouter"
)

func TestMachineDelete(t *testing.T) {
	machine := model.Machine{
		Id:     68574,
		Name:   "TestMachineDelete",
		Image:  "ubuntu/trusty",
		Driver: "lxd",
		UserId: int64(testToken.Claims.(jwt.MapClaims)["id"].(float64)),
		NodeId: "node",
	}

	if err := testDB.Insert(&machine); err != nil {
		t.Fatal(err)
	}

	mockScheduler.Machines[machine.Id] = "node"

	r, err := http.NewRequest("DELETE", fmt.Sprintf("/machines/%v", machine.Id), nil)
	if err != nil {
		t.Fatal(err)
	}
	*r = *r.WithContext(context.WithValue(r.Context(), jwtmiddleware.TokenContextKey{}, testToken))

	rr := httptest.NewRecorder()
	router := httprouter.New()
	router.Handle("DELETE", "/machines/:id", testHandler.MachineDelete)
	router.ServeHTTP(rr, r)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	if _, ok := mockScheduler.Machines[machine.ID]; ok {
		t.Error("machine was not deleted")
	}
}
