package api

import (
	"net/http"

	"github.com/google/jsonapi"
)

var (
	// BadRequest for badly formatted requests
	BadRequest = Response{
		Status: http.StatusBadRequest,
		Error: &jsonapi.ErrorObject{
			Code:   "bad_request",
			Title:  "Bad Request",
			Detail: "Your request is not in a valid format",
		},
	}
	// InternalServerError for unhandled errors
	InternalServerError = &jsonapi.ErrorObject{
		Code:   "internal_server_error",
		Title:  "Internal server error",
		Detail: "Unexpected internal error",
	}
	// NotFound for non existing resources
	NotFound = Response{
		Status: http.StatusNotFound,
		Error: &jsonapi.ErrorObject{
			Code:   "not_found",
			Title:  "Resource was not found",
			Detail: "The resource you requested does not exist",
		},
	}
	// Unauthorized is returned when request is not authenticated
	Unauthorized = Response{
		Status: http.StatusUnauthorized,
		Error: &jsonapi.ErrorObject{
			Code:   "unauthorized",
			Title:  "Unauthorized",
			Detail: "The request lacks valid authentication credentials.",
		},
	}
	// AccessDenied is returned when rqeuested action is not allowed
	AccessDenied = Response{
		Status: http.StatusUnauthorized,
		Error: &jsonapi.ErrorObject{
			Code:   "access_denied",
			Title:  "Access denied",
			Detail: "You are not allowed to perform the requested action.",
		},
	}
	// ValidationFailed is returned when request did not pass validation
	ValidationFailed = Response{
		Status: http.StatusBadRequest,
		Error: &jsonapi.ErrorObject{
			Code:  "validation_failed",
			Title: "Request validation failed",
		},
	}
)
