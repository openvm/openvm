package api

import (
	"bytes"
	"encoding/json"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	"github.com/google/jsonapi"
	"github.com/julienschmidt/httprouter"
)

func TestUserCreate(t *testing.T) {
	signup := SignUpRequest{
		Email:    "foo@bar.com",
		Name:     strconv.Itoa(rand.Int()),
		Password: "hunterhunter",
	}
	payload, _ := jsonapi.Marshal(&signup)
	buf, _ := json.Marshal(payload)

	r, err := http.NewRequest("POST", "/user", bytes.NewBuffer(buf))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	router := httprouter.New()
	router.Handle("POST", "/user", testHandler.UserCreate)
	router.ServeHTTP(rr, r)

	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusCreated)
	}
}

func TestUserCreateShortPassword(t *testing.T) {
	signup := SignUpRequest{
		Email:    "bar@baz.com",
		Name:     strconv.Itoa(rand.Int()),
		Password: "hunter",
	}
	payload, _ := jsonapi.Marshal(&signup)
	buf, _ := json.Marshal(payload)

	r, err := http.NewRequest("POST", "/user", bytes.NewBuffer(buf))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	router := httprouter.New()
	router.Handle("POST", "/user", testHandler.UserCreate)
	router.ServeHTTP(rr, r)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusBadRequest)
	}
}
