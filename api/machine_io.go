package api

import (
	"io"
	"net/http"
	"strconv"

	"github.com/go-pg/pg"
	"github.com/gorilla/websocket"
	"gitlab.com/openvm/openvm/container"
	"gitlab.com/openvm/openvm/model"
)

var (
	sessions = make(map[int64]session)
)

type session struct {
	w       io.WriteCloser
	r       io.ReadCloser
	control chan container.SessionControlMessage
	user    int64
}

// MachinesIO exposes the io socket
func (h *Handler) MachineIO(r Request) Response {
	if !r.Authenticated {
		return Unauthorized
	}

	id, err := strconv.ParseInt(r.Params.ByName("id"), 10, 64)
	if err != nil {
		return BadRequest
	}

	e, ok := sessions[id]

	// initialize new session if it doesn't exist
	if !ok {
		machine := model.Machine{Id: id}
		if err := h.DB.Select(&machine); err != nil {
			if err != pg.ErrNoRows {
				return Response{InternalError: err}
			}
			return NotFound
		}

		if machine.UserId != r.User {
			return AccessDenied
		}

		inr, inw := io.Pipe()
		outr, outw := io.Pipe()
		control := make(chan container.SessionControlMessage)

		props := container.SessionProperties{
			Stdin:   inr,
			Stdout:  outw,
			Control: control,
			Width:   80,
			Height:  40,
		}

		if err := h.Container.Session(machine.Id, props); err != nil {
			return Response{InternalError: err}
		}

		e = session{
			w:       inw,
			r:       outr,
			control: control,
			user:    r.User,
		}
		sessions[id] = e
	}

	upgrader := websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			origin := r.Header.Get("Origin")
			for _, o := range h.AllowOrigins {
				if o == origin || o == "*" {
					return true
				}
			}
			return false
		},
	}

	conn, err := upgrader.Upgrade(r.Writer, r.Request, nil)
	if err != nil {
		return Response{InternalError: err}
	}
	defer conn.Close()

	go writePump(e.r, conn)
	readPump(e.w, conn)

	return Response{Status: http.StatusOK}
}

func readPump(w io.WriteCloser, conn *websocket.Conn) {
	for {
		mt, message, err := conn.ReadMessage()
		if err != nil {
			return
		}
		if mt != websocket.TextMessage {
			continue
		}

		if _, err := w.Write(message); err != nil {
			conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseGoingAway, "broken write pipe"))
			return
		}
	}
}

func writePump(r io.ReadCloser, conn *websocket.Conn) {
	for {
		p := make([]byte, 32*1024)

		n, err := r.Read(p)
		if err != nil {
			conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseGoingAway, "broken read pipe"))
			return
		}

		if err := conn.WriteMessage(websocket.TextMessage, p[0:n]); err != nil {
			return
		}
	}
}
