package api

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/google/jsonapi"
	jwtmiddleware "github.com/jgillich/jwt-middleware"
	"github.com/julienschmidt/httprouter"

	"gitlab.com/openvm/openvm/model"
)

func TestMachineCreate(t *testing.T) {
	machine := model.Machine{
		Name:   "TestMachineCreate",
		Image:  "ubuntu/trusty",
		Driver: "lxd",
	}

	payload, _ := jsonapi.Marshal(&machine)
	buf, _ := json.Marshal(payload)

	r, err := http.NewRequest("POST", "/machines", bytes.NewBuffer(buf))
	if err != nil {
		t.Fatal(err)
	}
	*r = *r.WithContext(context.WithValue(r.Context(), jwtmiddleware.TokenContextKey{}, testToken))

	rr := httptest.NewRecorder()
	router := httprouter.New()
	router.Handle("POST", "/machines", testHandler.MachineCreate)
	router.ServeHTTP(rr, r)

	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusCreated)
	}

	res := model.Machine{}
	if err := jsonapi.UnmarshalPayload(rr.Body, &res); err != nil {
		t.Fatal(err)
	}

	if _, ok := mockScheduler.Machines[res.ID]; !ok {
		t.Error("machine was not created")
	}
}
