package api

import (
	"strconv"

	"github.com/go-pg/pg"
	"github.com/google/jsonapi"
	"gitlab.com/openvm/openvm/model"
)

// MachineUpdate updates a machine
func (h *Handler) MachineUpdate(r Request) Response {
	if !r.Authenticated {
		return Unauthorized
	}

	id, err := strconv.ParseInt(r.Params.ByName("id"), 10, 64)
	if err != nil {
		return BadRequest
	}

	machine := model.Machine{Id: id}
	if err := h.DB.Select(&machine); err != nil {
		if err != pg.ErrNoRows {
			return Response{InternalError: err}
		}
		return NotFound
	}

	if machine.UserId != r.User {
		return AccessDenied
	}

	props := new(model.MachineProperties)
	if err := jsonapi.UnmarshalPayload(r.Body, props); err != nil {
		return BadRequest
	}

	if err := h.Container.Update(machine.Id, *props); err != nil {
		return Response{InternalError: err}
	}

	return Response{Status: 204}
}
