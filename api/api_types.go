package api

type MachinePost struct {
	Name  string `jsonapi:"attr,name" valid:"printableascii,length(4|50),required"`
	Image string `jsonapi:"attr,image" valid:"printableascii,length(4|50),required"`
}

type UserPost struct {
	Password string `jsonapi:"attr,password" valid:"stringlength(8|100),required"`
	Name     string `jsonapi:"attr,name" valid:"alphanum,stringlength(4|20),required"`
	Email    string `jsonapi:"attr,email" valid:"email,required"`
}

type UserLoginPost struct {
	Name     string   `jsonapi:"attr,name"`
	Password string   `jsonapi:"attr,password"`
	Claims   []string `jsonapi:"attr,claims"`
}

type UserLogin struct {
	Id    int64  `jsonapi:"primary,logins"`
	Token string `jsonapi:"attr,token"`
}

type Image struct {
	Slug     string   `jsonapi:"attr,slug"`
	Name     string   `jsonapi:"attr,name"`
	Versions []string `jsonapi:"attr,versions"`
}
