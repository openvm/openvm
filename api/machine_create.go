package api

import (
	"net/http"

	"github.com/hashicorp/go-multierror"

	"github.com/asaskevich/govalidator"

	"github.com/google/jsonapi"
	"gitlab.com/openvm/openvm/model"
)

var (
	// QuotaExceededError is returned when the machine limit for a user is reached
	QuotaExceededError = &jsonapi.ErrorObject{
		Code:   "machine_quota_exceeded",
		Title:  "Machine quota exceeded",
		Detail: "You have reached your limit of machines you are allowed to create.",
	}
)

// MachineCreate creates a new machine
func (h *Handler) MachineCreate(r Request) Response {
	if !r.Authenticated {
		return Unauthorized
	}

	// TODO
	quotas := map[string]int{
		"instances": 10,
		"cpu":       1,
		"ram":       1,
	}

	if count, err := h.DB.Model(&model.Machine{}).Where("user_id = ?", r.User).Count(); err != nil {
		return Response{InternalError: err}
	} else if count >= quotas["instances"] {
		return Response{Error: QuotaExceededError, Status: http.StatusForbidden}
	}

	post := new(MachinePost)
	if err := jsonapi.UnmarshalPayload(r.Body, post); err != nil {
		return BadRequest
	}

	if _, err := govalidator.ValidateStruct(post); err != nil {
		e := ValidationFailed
		e.Error.Detail = err.Error()
		return e
	}

	machine := model.Machine{
		Name:   post.Name,
		Image:  post.Image,
		UserId: r.User,
		Driver: "lxd",
	}

	if err := h.DB.Insert(&machine); err != nil {
		return Response{InternalError: err}
	}

	props := model.MachineProperties{
		Name:  post.Name,
		Image: post.Image,
	}

	err := h.Container.Create(machine.Id, props)
	if err != nil {
		if dbErr := h.DB.Delete(&machine); dbErr != nil {
			return Response{InternalError: multierror.Append(err, dbErr)}
		}
		return Response{InternalError: err}
	}

	status, stats, err := h.Container.Get(machine.Id)
	if err != nil {
		return Response{InternalError: err}
	}

	machine.Status = *status
	machine.Stats = *stats

	return Response{Resource: &machine, Status: http.StatusCreated}
}
