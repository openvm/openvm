package container

import (
	"fmt"

	"github.com/lxc/lxd/shared/api"
	"gitlab.com/openvm/openvm/model"
)

func (container *Container) Create(id int64, props model.MachineProperties) error {

	name := fmt.Sprintf("openvm-%v", id)

	/*config := map[string]string{
		"limits.cpu":           string(attrs.CPU),
		"limits.cpu.allowance": "10%",
		"limits.cpu.priority":  "0",
		"limits.disk.priority": "0",
		"limits.memory":        fmt.Sprintf("%vGB", attrs.RAM),
		"limits.processes":     "500",
	}*/

	req := api.ContainersPost{
		Name: name,
		Source: api.ContainerSource{
			Type:     "image",
			Alias:    props.Image,
			Server:   "https://images.linuxcontainers.org",
			Protocol: "simplestreams",
		},
	}

	op, err := container.server.CreateContainer(req)
	if err != nil {
		return err
	}

	return op.Wait()
}
